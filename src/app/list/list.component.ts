import { Component, OnInit } from '@angular/core';
import DataService from '../data.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private dataService: DataService) { }

  list:Object[];

  ngOnInit() {
    this.list = this.dataService.getDataList();
  }

}
