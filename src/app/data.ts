export const data = {
	"url": "http://167.99.183.8/cnpjsearch/5/",
	"jucespreport_set": [
		{
			"url": "http://167.99.183.8/jucespreport/4/",
			"created": "2019-07-16T20:33:49.297345Z",
			"status": "CP",
			"emissao": "16/07/2019 17:34:03",
			"razao": "DAGOBERTO CARLOS DA CRUZ",
			"nire": "35109121154",
			"tipo": "EMPRESÁRIO (M.E.)",
			"data_da_constituicao": "10/06/1988",
			"inicio_de_atividade": "10/06/1988",
			"cnpj": "59.066.183/0001-90",
			"inscricao_estadual": "",
			"objeto": "Comercio e beneficiamento de generos alimenticios embalados(amendoim castanhas, sementes de abobora)",
			"capital": "R$ 2.000,00 (Dois Mil Reais)",

			"logradouro": "Rua Padre Francisco Barreto",
			"numero": "141",
			"bairro": "Vila Jaguara",
			"complemento": "",
			"municipio": "São Paulo",
			"cep": "05118-050",
			"uf": "SP",
			"search": "http://167.99.183.8/cnpjsearch/5/"
		}	
	],
	"receitacnpjreport_set": [
		{
			"url": "http://167.99.183.8/receitacnpjreport/5/",
			"created": "2019-07-16T20:33:49.224110Z",
			"status": "CP",
			"cnpj": "59.066.183/0001-90",
			"matriz": "MATRIZ",

			"abertura": "10/06/1988",
			"razao": "DAGOBERTO CARLOS DA CRUZ",
			"fantasia": "CAJU VANGUARDA",
			"porte": "ME",
			"ativ_econ_prin": "10.69-4-00 - Moagem e fabricação de produtos de origem vegetal não especificados anteriormente",
			"ativ_econ_sec": "Não informada",
			"nat_jur": "213-5 - Empresário (Individual)",

			"logradouro": "R PADRE FRANCISCO BARRETO",
			"numero": "141",
			"complemento": "",
			"cep": "05.118-050",
			"bairro": "VILA JAGUARA",
			"municio": "SAO PAULO",
			"uf": "SP",

			"email": "contabilhabber@uol.com.br",
			"telefone": "(11) 2605-8303 / (11) 2605-6442",

			"efr": "*****",
			"situacao": "ATIVA",
			"data_situacao": "03/11/2005",
			"motivo_situacao": "",
			"situacao_esp": "********",
			"data_sit_esp": "********",

			"emitido": "16/07/2019 17:33:56",
			"search": "http://167.99.183.8/cnpjsearch/5/"
		}
	],
	"receitadadospublicosempresareport_set": [
		{
			"url": "http://167.99.183.8/receitadadospubempresareport/2/",
			"created": "2019-07-16T20:33:49.367647Z",
			"status": "CP",
			"search": "http://167.99.183.8/cnpjsearch/5/",
			"empresas": [
				{
					"url": "http://167.99.183.8/receitadadospubempresa/37060214/",
					"created": "2019-07-15T04:26:09.079060Z",
					"cnpj": "59066183000190",
					"matriz_filial": "1",
					"razao_social": "DAGOBERTO CARLOS DA CRUZ",
					"nome_fantasia": "CAJU VANGUARDA",
					"situacao": "02",
					"data_situacao": "20051103",
					"motivo_situacao": "00",
					"nm_cidade_exterior": "",
					"cod_pais": "",
					"nome_pais": "",
					"cod_nat_juridica": "2135",
					"data_inicio_ativ": "19880610",
					"cnae_fiscal": "1069400",
					"tipo_logradouro": "RUA",
					"logradouro": "PADRE FRANCISCO BARRETO",
					"numero": "141",
					"complemento": "",
					"bairro": "VILA JAGUARA",
					"cep": "05118050",
					"uf": "SP",
					"cod_municipio": "7107",
					"municipio": "SAO PAULO",

					"ddd_1": "11",
					"telefone_1": "26058303",
					"ddd_2": "11",
					"telefone_2": "26056442",
					"ddd_fax": "11",
					"num_fax": "26058132",
					"email": "contabilhabber@uol.com.br",

					"qualif_resp": "50",
					"capital_social": "00000000000000",
					"porte": "01",
					"opc_simples": "5",
					"data_opc_simples": "20180101",
					"data_exc_simples": "",
					"opc_mei": "N",
					"sit_especial": "",
					"data_sit_especial": "",
					"externaldatasource": "http://167.99.183.8/externaldatasource/2/",
					"pessoajuridica": "http://167.99.183.8/pessoajuridica/59066183000190/"
				}
			]
		}
	],
	"created": "2019-07-16T20:33:48.972349Z",
	"cnpj": "59.066.183/0001-90",
	"razao_social": "DAGOBERTO CARLOS DA CRUZ ME"
}
