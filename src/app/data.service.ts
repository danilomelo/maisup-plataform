import { Injectable } from '@angular/core';
import { data } from './data';

@Injectable()
export default class DataService{
	getDataList(){
		return [data];
	}

	getDataDetails(id:string){
		return data;
	}
}