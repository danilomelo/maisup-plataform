import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import DataService from '../data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  constructor(private router:ActivatedRoute, private dataService:DataService) { }

  data:Object;

  ngOnInit() {
    this.router.paramMap.subscribe(
      (params) => this.getDataDetails(params.get('id'))
    )
  }

  getDataDetails(id:string){
    this.data = this.dataService.getDataDetails(id);
  }

}
